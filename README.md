Mapper
======

<h3>Code Climate</h3>
<a href="https://codeclimate.com/repos/5313ca5ce30ba0768e0000eb/feed">
    <img src="https://codeclimate.com/repos/5313ca5ce30ba0768e0000eb/badges/ced2362d8e47d6a3c8e7/gpa.png" />
</a>
<h2>Usage</h2>
<ul>
<li><b>rake spec</b> # &lt;= starts RSpec tests</li>
<li><b>rake start_solr</b> # &lt;= starts Solr</li>
<li><b>rake stop_solr</b> # &lt;= stops Solr</li>
<li><b>rake run</b> # &lt;= runs Mapper</li>
</ul>
<h2>Configs</h2>
<h3>TeamCity</h3>
<ul>
<li>home : /home/nychos/TeamCity</li>
<li>port: 8111</li>
<li>address: <b>88.198.94.236:8111</b></li>
<li>login: admin</li>
<li>password: uteam</li>
</ul>
